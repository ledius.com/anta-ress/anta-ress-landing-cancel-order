FROM node:16 as build-stage
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run build

FROM nginx:latest as production
COPY --from=build-stage /app /usr/share/nginx/html
